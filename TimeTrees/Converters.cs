﻿using System;
using System.Globalization;

namespace TimeTrees
{
    public static class Converters
    {
        public static string ConvertDateToFormatStr(DateTime date)
        {
            string year = date.Year.ToString();
            string month = date.Month.ToString();
            string day = date.Day.ToString();

            while (year.Length < 4)
                year = $"0{year}";
            while (month.Length < 2)
                month = $"0{month}";
            while (day.Length < 2)
                day = $"0{day}";

            return $"{year}-{month}-{day}";
        }

        public static DateTime ConvertStrToDate(string date)
        {
            DateTime convertedStr;

            if (!DateTime.TryParseExact(date, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out convertedStr))
            {
                if (!DateTime.TryParseExact(date, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out convertedStr))
                {
                    if (!DateTime.TryParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out convertedStr))
                    {
                        throw new FormatException("Incorrect format");
                    }
                }
            }

            return convertedStr;
        }
    }
}
