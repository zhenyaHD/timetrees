﻿using System;
using System.IO;

namespace TimeTrees
{
    public static class PersonHelper
    {
        public static int GetMaxId()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "people.csv");
            string[] data = File.ReadAllLines(path);

            return data.Length;
        }
    }
}
