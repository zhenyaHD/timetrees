﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TimeTrees
{
    public static class WriteData
    {
        public static void AddNewTimeLineCsv(TimelineEvent timeline)
        {
            string timelinePath = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string participantsPath = Path.Combine(Environment.CurrentDirectory, "participants.csv");

            string timelineData;
            string peopleId = string.Empty;

            string strDate = Converters.ConvertDateToFormatStr(timeline.Date);
            timelineData = $"{strDate};{timeline.EventName}";

            if (timeline.PeopleId.Count == 0) peopleId = "null";
            else
            {
                foreach (int id in timeline.PeopleId)
                    peopleId += $"{id};";
                peopleId = peopleId.Remove(peopleId.Length - 1, 1);
            }

            File.AppendAllText(timelinePath, $"{timelineData}\n");
            File.AppendAllText(participantsPath, $"{peopleId}\n");
        }

        public static void AddNewPersonCsv(Person person)
        {
            string peoplePath = Path.Combine(Environment.CurrentDirectory, "people.csv");
            string parentsPath = Path.Combine(Environment.CurrentDirectory, "parents.csv");

            string personData;
            string parentsId = string.Empty;

            string strBirthDate = Converters.ConvertDateToFormatStr(person.BirthDate);

            personData = $"{person.Identificator};{person.Name};{strBirthDate}";

            if (person.DeathDate != null)
            {
                string strDeathDate = Converters.ConvertDateToFormatStr(person.DeathDate.Value);
                personData += $";{strDeathDate}";
            }

            if (person.Parents == null) parentsId = "null";
            else
            {
                foreach (int id in person.Parents)
                    parentsId += $"{id};";
                parentsId = parentsId.Remove(parentsId.Length - 1, 1);
            }

            File.AppendAllText(peoplePath, $"{personData}\n");
            File.AppendAllText(parentsPath, $"{parentsId}\n");
        }

        public static void AddNewTimeLineJson(TimelineEvent timeline)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "timeline.json");
            string json = File.ReadAllText(path);

            List<TimelineEvent> timelines = JsonConvert.DeserializeObject<List<TimelineEvent>>(json);
            timelines.Add(timeline);

            json = JsonConvert.SerializeObject(timelines, Formatting.Indented);
            File.WriteAllText(path, json);
        }

        public static void AddNewPersonJson(Person person)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "people.json");
            string json = File.ReadAllText(path);

            List<Person> people = JsonConvert.DeserializeObject<List<Person>>(json);
            people.Add(person);

            json = JsonConvert.SerializeObject(people, Formatting.Indented);
            File.WriteAllText(path, json);
        }

        public static void ReWriteDataCsv(List<Person> people)
        {
            string peoplePath = Path.Combine(Environment.CurrentDirectory, "people.csv");
            string parentsPath = Path.Combine(Environment.CurrentDirectory, "parents.csv");

            string[] peopleData = new string[people.Count];
            string[] parentsId = new string[people.Count];

            for (int i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                string idLine = string.Empty;

                if (person.DeathDate != null)
                {
                    peopleData[i] = $"{person.Identificator}"
                                  + $";{person.Name}"
                                  + $";{Converters.ConvertDateToFormatStr(person.BirthDate)}"
                                  + $";{Converters.ConvertDateToFormatStr(person.DeathDate.Value)}";
                }
                else
                {
                    peopleData[i] = $"{person.Identificator}"
                                  + $";{person.Name}"
                                  + $";{Converters.ConvertDateToFormatStr(person.BirthDate)}";
                }

                if (person.Parents != null)
                {
                    foreach (int id in person.Parents)
                    {
                        idLine += $"{id};";
                    }
                    parentsId[i] = idLine.Remove(idLine.Length - 1);
                }
                else
                {
                    parentsId[i] = "null";
                }

                File.WriteAllLines(peoplePath, peopleData);
                File.WriteAllLines(parentsPath, parentsId);
            }
        }

        public static void ReWriteDataJson(List<Person> people)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "people.json");
            string json = JsonConvert.SerializeObject(people, Formatting.Indented);

            File.WriteAllText(path, json);
        }
    }
}
