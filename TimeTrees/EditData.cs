﻿using System.Collections.Generic;

namespace TimeTrees
{
    public static class EditData
    {
        public static void EditDataPeople()
        {
            Person selectedPerson = FindProgram.FindPersonMenu();
            Person newPerson = new Person();

            List<Person> people = ReadData.ReadPeopleCsv();
            int index = selectedPerson.Identificator - 1;

            newPerson.Identificator = selectedPerson.Identificator;
            newPerson.Name = InputData.NamePersonInput();

            newPerson.BirthDate = InputData.BirthDateInput();
            newPerson.DeathDate = InputData.DeathDateInput(newPerson.BirthDate);
            newPerson.Parents = InputData.ParentsIdInput();

            people[index] = newPerson;
            WriteData.ReWriteDataCsv(people);
            WriteData.ReWriteDataJson(people);
        }
    }
}
