﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class DifferenceDatesProgram
    {
        public static void PrintDifferenceDates(string extension)
        {
            List<TimelineEvent> timeline = extension == "Csv"
                ? ReadData.ReadTimeLineCsv()
                : ReadData.ReadTimeLineJson();

            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            (int years, int months, int days) = GetDifferenceDates(minDate, maxDate);

            Console.WriteLine($"Разница между макс и мин датами: {years} лет, {months} месяцев и {days} дней.");
            Console.ReadKey();
        }

        public static (int, int, int) GetDifferenceDates(DateTime minDate, DateTime maxDate)
        {
            int years = maxDate.Year - minDate.Year;
            int months = maxDate.Month - minDate.Month;
            int days = maxDate.Day - minDate.Day;

            (months, days) = CorrectDays(maxDate, months, days);
            (years, months) = CorrectMonths(years, months);

            return (years, months, days);
        }

        public static (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (TimelineEvent tl in timeline)
            {
                DateTime currentDate = tl.Date;

                if (minDate > currentDate) minDate = currentDate;
                if (maxDate < currentDate) maxDate = currentDate;
            }

            return (minDate, maxDate);
        }

        public static (int, int) CorrectDays(DateTime maxDate, int months, int days)
        {
            if (days < 0)
            {
                if (maxDate.Month == 4 || maxDate.Month == 6 || maxDate.Month == 9 || maxDate.Month == 11)
                {
                    days += 30;
                    months -= 1;
                }
                else if (maxDate.Month == 2 && (DateTime.IsLeapYear(maxDate.Year)))
                {
                    days += 29;
                    months -= 1;
                }
                else if (maxDate.Month == 2)
                {
                    days += 28;
                    months -= 1;
                }
                else
                {
                    days += 31;
                    months -= 1;
                }
            }

            return (months, days);
        }

        public static (int, int) CorrectMonths(int years, int months)
        {
            if (months < 0)
            {
                months += 12;
                years -= 1;
            }

            return (years, months);
        }
    }
}
