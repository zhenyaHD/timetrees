﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class FindProgram
    {
        public static void PrintPeople(List<Person> people, int? selectedIndex)
        {
            for (int i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                string name = person.Name;

                if (selectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }

                while (name.Length < 40)
                {
                    name += " ";
                }

                Console.WriteLine($"{person.Identificator}\t" +
                                  $"{name}" +
                                  $"{person.BirthDate.ToShortDateString()}\t" +
                                  $"{(person.DeathDate.HasValue ? person.DeathDate.Value.ToShortDateString() : "жив       ")}");

                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        public static List<Person> FilterPeople(List<Person> people, string name)
        {
            List<Person> filteredPeople = new List<Person>();
            foreach (Person person in people)
            {
                if (person.Name.Contains(name))
                {
                    filteredPeople.Add(person);
                }
            }

            return filteredPeople;
        }

        public static Person FindPersonMenu()
        {
            List<Person> people = ReadData.ReadPeopleCsv();

            List<Person> found = new List<Person>();
            string text = "Начните вводить имя: ";
            string name = string.Empty;
            int? selectedIndex = null;
            Person selectedPerson = null;

            do
            {
                ConsoleHelper.ScrenClear();

                Console.WriteLine("ПОИСК ЛЮДЕЙ");
                Console.WriteLine($"{text}{name}");
                Console.CursorVisible = true;

                found = string.IsNullOrEmpty(name)
                    ? people
                    : FilterPeople(people, name);

                PrintPeople(found, selectedIndex);

                Console.SetCursorPosition($"{text}{name}".Length, 1);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                Console.CursorVisible = false;

                if (char.IsLetter(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedIndex = null;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    if (name.Length != 0)
                    {
                        name = name.Remove(name.Length - 1, 1);
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex.HasValue)
                    {
                        selectedPerson = people[selectedIndex.Value];
                        break;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? found.Count - 1
                        : selectedIndex == 0
                            ? found.Count - 1
                            : selectedIndex - 1;
                }
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? 0
                        : selectedIndex == found.Count - 1
                            ? 0
                            : selectedIndex + 1;
                }
            } while (true);

            Console.CursorVisible = false;
            return selectedPerson;
        }
    }
}
