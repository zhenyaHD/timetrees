﻿namespace TimeTrees
{
    public class MenuItem
    {
        public string Id;
        public string Text;
        public bool IsSelected;
    }
}
