﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public class TimelineEvent
    {
        public DateTime Date;
        public string EventName;
        public List<int> PeopleId;
    }
}
