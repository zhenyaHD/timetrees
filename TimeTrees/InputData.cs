﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class InputData
    {
        public static string NamePersonInput()
        {
            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.Write("Введите имя: ");
                Console.CursorVisible = true;

                string name = Console.ReadLine();
                Console.CursorVisible = false;

                try
                {
                    if (name == "") throw new Exception();
                    return name;
                }
                catch (Exception)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Поле не должно быть пустым!");
                    Console.ReadKey();
                }
            }
        }

        public static string NameEventInput()
        {
            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.Write("Введите название события: ");
                Console.CursorVisible = true;

                string name = Console.ReadLine();
                Console.CursorVisible = false;

                try
                {
                    if (name == "") throw new Exception();
                    return name;
                }
                catch (Exception)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Поле не должно быть пустым!");
                    Console.ReadKey();
                }
            }
        }

        public static DateTime BirthDateInput()
        {
            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.Write("Введите дату рождения гггг-мм-дд: ");
                Console.CursorVisible = true;

                string strDate = Console.ReadLine();
                Console.CursorVisible = false;

                try
                {
                    DateTime date = Converters.ConvertStrToDate(strDate);

                    return date;
                }
                catch (FormatException)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Вы неверно ввели дату!");
                    Console.ReadKey();
                }
            }
        }

        public static DateTime? DeathDateInput(DateTime birthDate)
        {
            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.Write("Введите дату смерти, если она есть гггг-мм-дд: ");
                Console.CursorVisible = true;

                string strDate = Console.ReadLine();
                Console.CursorVisible = false;

                try
                {
                    if (strDate != string.Empty)
                    {
                        DateTime deathDate = Converters.ConvertStrToDate(strDate);
                        if (birthDate > deathDate) throw new Exception();

                        return deathDate;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (FormatException)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Вы неверно ввели дату!");
                    Console.ReadKey();
                }
                catch (Exception)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Дата смерти не можеть быть меньше даты рождения");
                    Console.ReadKey();
                }
            }
        }

        public static DateTime EventDateInput()
        {
            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.Write("Введите дату события гггг-мм-дд: ");
                Console.CursorVisible = true;

                string strDate = Console.ReadLine();
                Console.CursorVisible = false;

                try
                {
                    DateTime date = Converters.ConvertStrToDate(strDate);
                    return date;
                }
                catch (FormatException)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Вы неверно ввели дату");
                    Console.ReadKey();
                }
            }
        }

        public static List<int> PeopleIdInput()
        {
            List<int> peopleId = new List<int>();

            while (true)
            {
                ConsoleHelper.ScrenClear();

                Console.WriteLine("Нажмите Enter чтобы продолжить");
                Console.WriteLine("Нажмите A чтобы добавить участника события");

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                Person person = new Person();

                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                        if (peopleId.Count == 0) return null;
                        else return peopleId;
                    case ConsoleKey.A:
                        person = FindProgram.FindPersonMenu();
                        break;
                    default:
                        continue;
                }

                try
                {
                    if (person != null)
                    {
                        if (peopleId.Contains(person.Identificator))
                        {
                            throw new Exception("The same person");
                        }
                        peopleId.Add(person.Identificator);
                    }
                }
                catch (Exception)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Человека с таким айди не существует, или вы повтороно ввели один и тот же айди");
                    Console.ReadKey();
                }
            }
        }

        public static List<int> ParentsIdInput()
        {
            List<int> parentsId = new List<int>();

            while (true)
            {
                ConsoleHelper.ScrenClear();

                if (parentsId.Count == 2) return parentsId;

                Console.WriteLine("Нажмите Enter чтобы продолжить");
                Console.WriteLine("Нажмите A чтобы добавить родителя");

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                Person person = new Person();

                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                        if (parentsId.Count == 0) return null;
                        else return parentsId;
                    case ConsoleKey.A:
                        person = FindProgram.FindPersonMenu();
                        break;
                    default:
                        continue;
                }

                try
                {
                    if (person != null)
                    {
                        if (parentsId.Contains(person.Identificator))
                        {
                            throw new Exception("The same person");
                        }
                        parentsId.Add(person.Identificator);
                    }
                }
                catch (Exception)
                {
                    ConsoleHelper.ScrenClear();

                    Console.WriteLine("Нельзя выбрать одного и того же человека дважды!");
                    Console.ReadKey();
                }
            }
        }
    }
}
