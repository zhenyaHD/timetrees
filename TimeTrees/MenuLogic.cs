﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TimeTrees
{
    public static class MenuLogic
    {
        /// <summary>
        /// Вывести список событий
        /// </summary>
        private const string TimeLineListId = "TimeLineList";

        /// <summary>
        /// Вывести список людей
        /// </summary>
        private const string PeopleListId = "PeopleList";

        /// <summary>
        /// Найти разницу дат между событиями
        /// </summary>
        private const string DifferenceDatesId = "Difference";

        /// <summary>
        /// Найти людей, родившихся в високосный год
        /// </summary>
        private const string BornInLeapYearId = "LeapYear";

        /// <summary>
        /// Вести данные для нового события
        /// </summary>
        private const string NewTimelineId = "NewTimeLine";

        /// <summary>
        /// Ввести данные для нового человека
        /// </summary>
        private const string NewPersonId = "NewPerson";

        /// <summary>
        /// Выбрать расширение csv
        /// </summary>
        private const string CsvId = "Csv";

        /// <summary>
        /// Выбрать расширение json
        /// </summary>
        private const string JsonId = "Json";

        /// <summary>
        /// Вернуться назад
        /// </summary>
        private const string BackId = "Back";

        /// <summary>
        /// Выйти из программы
        /// </summary>
        private const string ExitId = "Exit";

        /// <summary>
        /// Редактировать данные
        /// </summary>
        private const string EditId = "Edit";

        static void Main()
        {
            Console.CursorVisible = false;
            List<MenuItem> mainMenu = new List<MenuItem>
            {
                new MenuItem { Id = TimeLineListId, Text = "Вывести cписок событий", IsSelected = true},
                new MenuItem { Id = PeopleListId, Text = "Вывести список людей"},
                new MenuItem { Id = DifferenceDatesId, Text = "Разница между макс и мин датами"},
                new MenuItem { Id = BornInLeapYearId, Text = "Родившиеся в високосный год"},
                new MenuItem { Id = NewTimelineId, Text = "Ввести данные о новом событии"},
                new MenuItem { Id = NewPersonId, Text = "Ввести данные о новом человеке"},
                new MenuItem { Id = EditId, Text = "Редактировать данные"},
                new MenuItem { Id = ExitId, Text = "Выход",},
            };

            MainMenuControler(mainMenu);
        }

        static void SelectExtensionMenu(string id)
        {
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem { Id = CsvId, Text = "Csv", IsSelected = true },
                new MenuItem { Id = JsonId, Text = "Json"},
                new MenuItem { Id = BackId, Text = "Назад"},
            };

            SelectExtensionMenuControler(menu, id);
        }

        static void MainMenuControler(List<MenuItem> mainMenu)
        {
            bool exit = false;
            do
            {
                ConsoleHelper.ScrenClear();
                DrawMenu(mainMenu);

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        MenuSelectPrev(mainMenu);
                        break;
                    case ConsoleKey.DownArrow:
                        MenuSelectNext(mainMenu);
                        break;
                    case ConsoleKey.Enter:
                        MenuItem selectedItem = mainMenu.First(x => x.IsSelected);

                        if (selectedItem.Id == ExitId) exit = true;
                        else Execude(selectedItem.Id);

                        ConsoleHelper.ScrenClear();
                        break;

                }

            } while (!exit);
        }

        static void SelectExtensionMenuControler(List<MenuItem> menu, string id)
        {
            bool exit = false;
            do
            {
                ConsoleHelper.ScrenClear();

                Console.WriteLine("Выберите расширение файла\n");
                DrawMenu(menu);

                ConsoleKeyInfo keyIndo = Console.ReadKey(true);
                switch (keyIndo.Key)
                {
                    case ConsoleKey.UpArrow:
                        MenuSelectPrev(menu);
                        break;
                    case ConsoleKey.DownArrow:
                        MenuSelectNext(menu);
                        break;
                    case ConsoleKey.Enter:
                        ConsoleHelper.ScrenClear();

                        MenuItem selectedItem = menu.First(x => x.IsSelected);
                        if (selectedItem.Id != BackId)
                        {
                            if (id == DifferenceDatesId)
                            {
                                DifferenceDatesProgram.PrintDifferenceDates(selectedItem.Id);
                            }
                            else if (id == BornInLeapYearId)
                            {
                                LeapYearProgram.PrintBornInleapYear(selectedItem.Id);
                            }
                        }

                        exit = true;
                        break;
                }
            } while (!exit);
        }


        static void MenuSelectPrev(List<MenuItem> menu)
        {
            MenuItem selectedItem = menu.First(x => x.IsSelected);
            int selectIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectIndex = selectIndex == 0
                ? menu.Count - 1
                : --selectIndex;

            menu[selectIndex].IsSelected = true;
        }

        static void MenuSelectNext(List<MenuItem> menu)
        {
            MenuItem selectedItem = menu.First(x => x.IsSelected);
            int selectIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectIndex = selectIndex == menu.Count - 1
                ? 0
                : ++selectIndex;

            menu[selectIndex].IsSelected = true;
        }

        static void DrawMenu(List<MenuItem> menu)
        {
            foreach (MenuItem item in menu)
            {
                Console.BackgroundColor = item.IsSelected
                    ? ConsoleColor.DarkRed
                    : ConsoleColor.Black;

                Console.WriteLine(item.Text);
            }
            Console.BackgroundColor = ConsoleColor.Black;
        }

        static void Execude(string comandId)
        {
            switch (comandId)
            {
                case DifferenceDatesId:
                    SelectExtensionMenu(comandId);
                    break;
                case BornInLeapYearId:
                    SelectExtensionMenu(comandId);
                    break;
                case NewTimelineId:
                    AddProgram.BuildNewTimeline();
                    break;
                case NewPersonId:
                    AddProgram.BuildNewPerson();
                    break;
                case EditId:
                    EditData.EditDataPeople();
                    break;
                case TimeLineListId:
                    ConsolePrint.PrintTimeLineList();
                    break;
                case PeopleListId:
                    ConsolePrint.PrintPeopleList();
                    break;
            }
        }
    }
}