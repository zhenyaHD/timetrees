﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class AddProgram
    {
        public static void BuildNewTimeline()
        {
            while (true)
            {
                TimelineEvent newTimeLine = new TimelineEvent();

                string name = InputData.NameEventInput();
                DateTime date = InputData.EventDateInput();
                List<int> peopleId = InputData.PeopleIdInput();

                newTimeLine.EventName = name;
                newTimeLine.Date = date;
                newTimeLine.PeopleId = peopleId;

                WriteData.AddNewTimeLineCsv(newTimeLine);
                WriteData.AddNewTimeLineJson(newTimeLine);
                break;
            }
        }

        public static void BuildNewPerson()
        {
            while (true)
            {
                Person newPerson = new Person();

                int id = PersonHelper.GetMaxId();
                newPerson.Identificator = ++id;

                string name = InputData.NamePersonInput();
                DateTime birthDate = InputData.BirthDateInput();

                DateTime? deathDate = InputData.DeathDateInput(birthDate);
                List<int> parentsId = InputData.ParentsIdInput();

                newPerson.Name = name;
                newPerson.BirthDate = birthDate;
                newPerson.DeathDate = deathDate;
                newPerson.Parents = parentsId;

                WriteData.AddNewPersonCsv(newPerson);
                WriteData.AddNewPersonJson(newPerson);
                break;
            }
        }
    }
}
