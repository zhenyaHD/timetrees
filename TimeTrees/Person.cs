﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public class Person
    {
        public int Identificator;
        public string Name;
        public DateTime BirthDate;
        public DateTime? DeathDate;
        public List<int> Parents;
    }
}
