﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace TimeTrees
{
    public static class ReadData
    {
        public static List<TimelineEvent> ReadTimeLineCsv()
        {
            string timelinePath = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string participantsPath = Path.Combine(Environment.CurrentDirectory, "participants.csv");

            string[] data = File.ReadAllLines(timelinePath);
            string[] identifiers = File.ReadAllLines(participantsPath);

            List<TimelineEvent> splitData = new List<TimelineEvent>();
            for (int i = 0; i < data.Length; i++)
            {
                List<int> peopleId = new List<int>();
                string[] splitLine = data[i].Split(';');
                string[] splitIdentifiers;

                if (identifiers[i] != "null")
                {
                    splitIdentifiers = identifiers[i].Split(';');
                    foreach (string id in splitIdentifiers)
                        peopleId.Add(int.Parse(id));
                }
                else peopleId = null;

                TimelineEvent timeLine = new TimelineEvent();
                timeLine.Date = Converters.ConvertStrToDate(splitLine[0]);
                timeLine.EventName = splitLine[1];
                timeLine.PeopleId = peopleId;

                splitData.Add(timeLine);
            }

            return splitData;
        }

        public static List<Person> ReadPeopleCsv()
        {
            string peoplePath = Path.Combine(Environment.CurrentDirectory, "people.csv");
            string parentsPath = Path.Combine(Environment.CurrentDirectory, "parents.csv");

            string[] data = File.ReadAllLines(peoplePath);
            string[] identifiers = File.ReadAllLines(parentsPath);

            List<Person> splitData = new List<Person>();
            for (int i = 0; i < data.Length; i++)
            {
                List<int> parentsId = new List<int>();
                string[] splitLine = data[i].Split(';');
                string[] splitIdentifiers;

                if (identifiers[i] != "null")
                {
                    splitIdentifiers = identifiers[i].Split(';');
                    foreach (string id in splitIdentifiers)
                        parentsId.Add(int.Parse(id));
                }
                else parentsId = null;

                Person person = new Person();
                person.Identificator = int.Parse(splitLine[0]);
                person.Name = splitLine[1];
                person.BirthDate = Converters.ConvertStrToDate(splitLine[2]);
                person.Parents = parentsId;

                if (splitLine.Length == 4)
                    person.DeathDate = Converters.ConvertStrToDate(splitLine[3]);
                else
                    person.DeathDate = null;

                splitData.Add(person);
            }

            return splitData;
        }

        public static List<TimelineEvent> ReadTimeLineJson()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "timeline.json");
            string json = File.ReadAllText(path);

            List<TimelineEvent> timeline = JsonConvert.DeserializeObject<List<TimelineEvent>>(json);

            return timeline;
        }

        public static List<Person> ReadPeopleJson()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "people.json");
            string json = File.ReadAllText(path);

            List<Person> people = JsonConvert.DeserializeObject<List<Person>>(json);

            return people;
        }
    }
}
