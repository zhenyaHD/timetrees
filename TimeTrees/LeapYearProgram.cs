﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class LeapYearProgram
    {
        public static List<string> GetBornInLeapYear(List<Person> people)
        {
            List<string> names = new List<string>();
            foreach (Person person in people)
            {
                int birthYear = person.BirthDate.Year;
                if (DateTime.IsLeapYear(birthYear))
                {
                    DateTime dateDeath = person.DeathDate == null
                        ? DateTime.Today
                        : person.DeathDate.Value;

                    (int age, int months, int days) = DifferenceDatesProgram.GetDifferenceDates(person.BirthDate, dateDeath);
                    if (age <= 20) names.Add(person.Name);
                }
            }

            return names;
        }

        public static void PrintBornInleapYear(string extension)
        {
            List<Person> people = extension == "Csv"
                ? ReadData.ReadPeopleCsv()
                : ReadData.ReadPeopleJson();

            List<string> names = GetBornInLeapYear(people);

            Console.WriteLine("Список имён:\n");
            foreach (string name in names)
                Console.WriteLine(name);

            Console.ReadKey();
        }
    }
}
