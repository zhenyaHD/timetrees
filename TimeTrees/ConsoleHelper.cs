﻿using System;

namespace TimeTrees
{
    public static class ConsoleHelper
    {
        public static void ScrenClear()
        {
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(new string(' ', Console.WindowWidth));
            }

            Console.SetCursorPosition(0, 0);
        }
    }
}

