﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    public static class ConsolePrint
    {
        public static void PrintTimeLineList()
        {
            ConsoleHelper.ScrenClear();

            List<TimelineEvent> timeLine = ReadData.ReadTimeLineCsv();
            Console.WriteLine("Название:                               Дата:");

            foreach (TimelineEvent tl in timeLine)
            {
                string text = tl.EventName;
                while (text.Length < 40)
                {
                    text += " ";
                }

                text += $"{Converters.ConvertDateToFormatStr(tl.Date)}";
                Console.WriteLine(text);
            }

            Console.ReadKey();
            ConsoleHelper.ScrenClear();
        }

        public static void PrintPeopleList()
        {
            ConsoleHelper.ScrenClear();

            List<Person> people = ReadData.ReadPeopleCsv();
            Console.WriteLine("Имя:                                    Дата рождения:  Дата смерти:");

            foreach (Person person in people)
            {
                string text = person.Name;
                while (text.Length < 40)
                {
                    text += " ";
                }

                text += $"{Converters.ConvertDateToFormatStr(person.BirthDate)}\t";
                text += $"{(person.DeathDate.HasValue ? Converters.ConvertDateToFormatStr(person.DeathDate.Value) : "жив")}";

                Console.WriteLine(text);
            }

            Console.ReadKey();
            ConsoleHelper.ScrenClear();
        }
    }
}
